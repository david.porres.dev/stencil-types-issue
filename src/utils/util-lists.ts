export const UTIL_COLOR_LIST = [
  'primary', //
  'black',
  'white',
  'gray-lightest',
  'gray-lighter',
  'gray-light',
  'gray-dark',
  'gray-darker',
  'gray-darkest',
];
export type UTIL_COLOR_LIST_TYPE = typeof UTIL_COLOR_LIST[number];

export const UTIL_LEVEL_LIST = [1, 2, 3, 4, 5, 6];
export type UTIL_LEVEL_LIST_TYPE = typeof UTIL_LEVEL_LIST[number];

export const UTIL_SPACING_TYPE_LIST = ['lg', 'md', 'sm'];
export type UTIL_SPACING_TYPE_LIST_TYPE = typeof UTIL_SPACING_TYPE_LIST[number];

export const UTIL_BUTTON_VARIANT_LIST = ['primary', 'secondary', 'important'];
export type UTIL_BUTTON_VARIANT_LIST_TYPE = typeof UTIL_SPACING_TYPE_LIST[number];
