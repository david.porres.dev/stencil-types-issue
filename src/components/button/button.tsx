import { Component, Prop, h } from '@stencil/core';
import { UTIL_BUTTON_VARIANT_LIST, UTIL_BUTTON_VARIANT_LIST_TYPE, UTIL_SPACING_TYPE_LIST, UTIL_SPACING_TYPE_LIST_TYPE } from '../../utils/util-lists';
import classNames from 'classnames';

/**
 * @slot default - The label of the button
 */
@Component({
  tag: 'oms-button',
  styleUrl: '_button.scss',
  shadow: true,
})
export class Button {
  /**
   * The button href
   */
  @Prop() href: string = '#';

  /**
   * Style variation of the button.
   */
  @Prop() variant: UTIL_BUTTON_VARIANT_LIST_TYPE = UTIL_BUTTON_VARIANT_LIST[0];

  /**
   * The size of the button
   */
  @Prop() size: UTIL_SPACING_TYPE_LIST_TYPE = UTIL_SPACING_TYPE_LIST[1];

  render() {
    const variantClasses = 'oms-button--variant-' + this.variant;
    const sizeClasses = 'oms-button--size-' + this.size;

    return (
      <a class={classNames('oms-button', variantClasses, sizeClasses)} href={this.href}>
        <slot></slot>
      </a>
    );
  }
}
