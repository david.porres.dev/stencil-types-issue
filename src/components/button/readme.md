# oms-button



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                    | Type     | Default                       |
| --------- | --------- | ------------------------------ | -------- | ----------------------------- |
| `href`    | `href`    | The button href                | `string` | `'#'`                         |
| `size`    | `size`    | The size of the button         | `string` | `UTIL_SPACING_TYPE_LIST[1]`   |
| `variant` | `variant` | Style variation of the button. | `string` | `UTIL_BUTTON_VARIANT_LIST[0]` |


## Slots

| Slot        | Description             |
| ----------- | ----------------------- |
| `"default"` | The label of the button |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
