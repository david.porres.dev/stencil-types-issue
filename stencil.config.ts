import { Config } from '@stencil/core';

import { sass } from '@stencil/sass';
import { postcss } from '@stencil/postcss';
import autoprefixer from 'autoprefixer';
import { inlineSvg } from 'stencil-inline-svg';

export const config: Config = {
  namespace: 'types-issue-aresrioja10',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
    },
  ],
  plugins: [
    sass(),
    inlineSvg(),
    postcss({
      plugins: [autoprefixer()],
    }),
  ],
};
